---
title: "Pieces of Eight"
subtitle: "A Monkey-Island Themed Draughts Game"
date: 'Candidate No: 236544'
geometry: "left=2cm,right=2cm,top=2cm,bottom=2cm"
---

# Pieces of Eight: A Monkey-Island Themed Draughts Game

## Abstract
> **Guybrush**: At least I’ve learnt something from all of this.
>
> **Elaine**: What’s that?
>
> **Guybrush**: Never pay more than 20 bucks for a computer game.

## Introduction
Our task was to construct a draughts game for a single human player against an AI which selects smart moves by using an adversarial search algorithm to explore the game tree along with a heuristic function to evaluate the best outcomes, allowing optimisation of its performance. The adversarial search algorithm implemented is minimax using alpha-beta pruning, although the heuristic used to perform static evaluations results in the algorithm performing more like a negamax implementation. Minimax is a recursive search algorithm deployed at each AI turn (and by the human if desired) to select a move that will result in the best outcome by exploring all possible pathways in the game tree. The algorithm works by assuming that any player initializing the search will always pursue the path of highest utility, while the opponent will always play to minimise and limit their opponent's result. As a comprehensive search of the tree for a game such as Draughts is computationally infeasible, minimax can search only to a specific depth or possible future state, such that it is limited in observing all possible outcomes and therefore cannot guarantee a perfect game. Alpha-beta pruning allows for the exploration of a dramatically cut-down search tree, increasing the ability of the algorithm to probe deeper into the future in pursuit of a best outcome. This is accomplished by sharing knowledge obtained about best outcome so far to prune entire subsections of the search tree that cannot possibly achieve an outcome better than the one already discovered.

Our code is composed of a front-end browser application implemented in HTML / CSS / JavaScript, and a back-end web server implemented in Python. The web server makes the game logic available to the UI through a web API using a python web server called Flask. The UI uses AJAX (Asynchronous JavaScript and XML) requests to obtain the relevant information for displaying the game state and rendering the board. The back-end code is composed of an API with 5 distinct routes along with three main classes:

- `Game` contains the core game logic, including our board state representation, a successor function - which computes all valid subsequent moves given a specified player and a game state - and a goal test that checks to see if either player has won the game.
- `Move` accounts for the dynamic within draughts that a single player move can consist of a series of transitions or hops depending on the availability of a transitions that capture opponent piece(s). `Move` caches the transitions made and stores a reference to its parent move. It acts as a node in a depth-first search algorithm.
- `Search` contains our implementation of the minimax algorithm with alpha-beta pruning, as well as a single heuristic function which is used to compute static evaluations, i.e. player outcomes, at the leaf nodes during the exploration of the game tree.

![Pieces of Eight is a Monkey-Island themed draughts game. A text field below the board logs game information, changes in game state and errors supplied about moves. Hints can be toggled, and display all possible moves available for the player. Cheats can be turned on to perform minimax search on behalf of the player to a fixed maximum depth of 10 and maximum number of static evaluations at 500,000.](./assets/game.png){ width=75% }

## Program

### State Representation
Our game's state can be conceived of abstractly as two 1-dimensional arrays - one for each player. Each element in a state array is an integer $n \in \{0, ..., 63\}$ representing the location or square on the board occupied by that piece. This state representation minimises the need to store unoccupied locations in memory as zero values without any overhead. In real terms, each player's state is implemented as a hash table or python dictionary (see `server/lib/game.py#L30,31`), whereby the key $= n$ (i.e. the location of the piece on the board), and the value corresponds to the piece's value, either a pawn $= 1$ or a queen $= 2$. We note that a draughts board is an $8 \times 8$ grid, of which only half the number of squares (black) compose valid areas in the game, and as such the range of integers corresponding to locations on the board $n$ could have just as easily been $n \in \{0, ..., 31\}$. However this state representation offered no improvement in compute time and required additional calculation to map $n$ to an 8x8 grid when rendering the board in the UI.

### Successor Function
Our successor function is composed of three separate python functions contained within the `Game` class and the `Move` class. The implementation ensures the following features:

- No invalid moves can be made by either player.
  - Moves are validated when the selected move is sent to the server (see `create_move` in `server/app.py`).
- Captures are enforced by limiting the scope of available moves when at least one capture move has been discovered (see `server/lib/game.py#L265,269`).
- Multi-leg captures are enabled using a depth-first search for subsequent captures.
- Crowning occurs when moves are applied to the game state (see `move.apply` and `move.apply_chain` in `server/lib/move.py#L104,130`).
  - Queen conversion at the relevant baseline occurs by setting the piece value to $2$.
  - 'Regicide' occurs by setting the capturing piece value to $2$ if the captured piece is a queen.

#### Neighbours
The function `game.neighbours` (see `server/lib/game.py`) computes all possible neighbouring squares according to a specific piece's location $s$, its controlling player and its equivalent value. Player 1 can only move down ($+$) the board, while Player 2 can only move up ($-$) the board, while queens on either side can move in all directions. Neighbours or destinations $d$ are calculated by $d = s \pm 7$ and $d = s \pm 9$ to the current location. These are limited to ensure they are actually on an immediate neighbouring row on the board, preventing invalid moves which would allow pieces to exit the board and appear on the other side two rows away.

#### Available Moves
The main successor function `game.available_moves` (see `server/lib/game.py#L210`) computes neighbouring squares for each piece of the current player in question, eliminating invalid moves i.e. when the square is occupied either by a friendly or by an opponent. When an opponent piece is occupying a square, we check to see if the square that extends along the same axis on that diagonal is empty. If so, a valid capture move $c$ has been discovered and all non-capture moves found thus far will be ignored when the function returns. Subsequent captures are then sought (see `server/lib/game.py#L259`).

#### Captures
The discovery of $c$ initializes the creation of a `Move` instance, a root node in a recursive depth first search (see `game.captures` function in `server/lib/game.py`) which explores a tree of potential subsequent capture moves. Each child `Move` created refers to its parent as well as storing an integer value for $s$, $d$ and $c$ on the board, and the equivalent values of the pieces at those locations. In order to prevent circular outcomes for queen pieces in this depth first search, the move is applied to the game state and subsequently reversed using these values after computing whether further captures are available. `game.captures` returns an array of all the leaf nodes discovered during the depth first search and are yielded as unique moves by the successor function, along with any other subsequently discovered capture chains for other pieces. As such each full capture chain is treated as a unique outcome, allowing for exploration during adversarial search for potentially two or more distinct capture chains which lead to diverging game outcomes. Furthermore this feature allows for players to make non-optimal moves such as failing to perform numerous captures available as part of a chain (see `move.limit_by_destination` in `server/lib/move.py#L75` and its use in `create_move` in `server/app.py#L123`) while still enforcing at least one capture.

### Minimax with Alpha-Beta Pruning
Our `Search` class implements the minimax algorithm using alpha-beta pruning on behalf of the player that calls the `search_moves` end-point (see `server/app.py#L83`). The algorithm is initialized by the maximising player who makes a clone of the current game state (see `server/lib/search.py#L52`). The maximising player then calls the successor function to determine available moves, applies those changes to the cloned state before immediately searching deeper and switching to the minimising player in a depth-first fashion. Minimax alternates between the two players allows exploration of the game tree until leaf nodes are reached at the maximum depth and a static evaluation is computed (see `server/lib/search.py#L104`). These values are then returned up the search tree, where explored moves are undone in the game state, and the search continues deeper along an alternate pathway until all possible (or delimited) pathways are explored. Alpha-beta pruning carries the best value discovered for each player so far across to evaluations further down sibling pathways in the game tree and allows an early exit when pathways in the tree will never be chosen. For example, when the maximising player discovers an outcome on a sibling node that is greater than the current best so far (stored in alpha), the minimising player will never choose that outcome, and as such any further exploration will either be lower - and not picked by the maximising player - or higher - and not picked by the minimising player. As such deeper exploration into the other branches at that level would be a waste of computation and thus pruned, enabling deeper search in areas of the game tree that may yield more optimal outcomes for the maximising player. This is implemented by breaking when `alpha >= beta` when searching at a specific depth level (see `minimax_alpha_beta` in `server/lib/search.py#L146`), and sibling moves yielded by the successor function are skipped. Following the search, the best move is then selected and the returned the UI, whereby the AI then applies this move via a second request (see `ui/component/Board.vue#L305` and `server/lib/app.py#L109`).

Difficulties are implemented by combining two techniques. First we adjust the `max_depth` parameter for the search algorithm, limiting how far into the game tree the algorithm explores. Secondly, we place an upper bound on the number of static evaluations that are computed, potentially causing the best outcomes at the specified maximum depth in the game tree to remain undiscovered once the threshold is reached. Precise values are contained within the two functions `__max_depth__` and `__max_evaluations__` (see `server/lib/search.py#L28,36`), detailed below:

|        | Max Depth | Max Evaluations |
|--------|-----------|-----------------|
| Easy   | 1         | 250             |
| Medium | 5         | 10,000          |
| Hard   | 10        | 500,000         |

### Heuristic Function
We deploy a heuristic function during adversarial search to compute static evaluations at the leaf nodes of the search tree, i.e. when the maximum depth has been reached, either specified through the maximum depth parameter or when the search tree is limited by the number of moves remaining in the game. The maximising player seeks to increase the total _value_ of pieces it has, and the total _value_ of pieces its opponent has, while the minimising player seeks to increase the negative total _value_ of pieces it has and the total _value_ of pieces its opponent has. By calculating according to value, our heuristic enables the AI to take advantage of certain situations and make moves which incentivise the following outcomes:

1. Capture of a single or multiple opponent pieces
2. Defence of pieces that are under threat
3. Crowning of a queen
4. Capture of an opponents queen
5. Prioritising defence of a queen over a pawn

As a result of this heuristic, at the onset of the game our AI is encouraged over the long-term to position its pieces strategically and attempt to force opposing player to make the first mistake. However, this heuristic appears to perform somewhat poorly towards the end of game when players have lost a significant number of pieces and are relatively evenly matched. The above heuristic results in an effective equivalence between minimax and negamax, in so much as each player, whether minimising or maximising, is attempting to increase the total value of their pieces while reducing that of their opponent.

### UI
- Our UI implements drag and drop and staggered capture moves with a 500ms timeout.
- 'Hints' are toggle-able which highlight valid moves for the player including captures
- 'Cheats' may be switched on which performs adversarial search on the players behalf and highlights the suggested best move.

## Appendix

Python version used for development was `v3.8.11`.
NodeJS version used for development was `v16.5.0`

### Setup

If the `dist/` directory is missing or the UI is having issues, you'll need to recompile the UI. This requires NodeJS to be installed and a package manager. We recommend installing first [nvm](https://github.com/nvm-sh/nvm), then node, then proceeding with the following commands:

```bash
# install dependencies
npm --prefix ui install
# build the UI to the ./dist folder
npm --prefix ui run build
```

To start the app:

```bash
# ensure necessary python dependencies
# flask is a lightweight web server and application framework
pip install flask flask-cors
pip install -U pytest

# run the tests
python tests.py

# start the app
python main.py
```

### UI

The UI is built using a framework called VueJS. This is the project structure:

```bash
├── App.vue
├── assets
│   ├── css
│   ├── fonts
│   ├── img
│   └── index.html
├── babel.config.js
├── components
│   ├── Board.vue
│   └── Draught.vue
├── main.js
├── node_modules
├── package.json
├── pnpm-lock.yaml
├── routes.js
├── views
│   ├── Game.vue
│   ├── Home.vue
│   └── Rules.vue
└── vue.config.js
```

I have included only the two main game components`Board.vue` and `Draught.vue` which contain all the relevant information for the game implementation. The rest including configuration files and styling, etc, is contained within the code provided in the zip archive.

```html
<!-- ui/components/Board.vue -->

<template>
 <div class="board-container">
    <div class="board">
      <div v-for="(draught, cellId) in board"
        class="tile"
        :id="cellId"
        :key="cellId"
        :class="[defaultTileColour(cellId), isHint(cellId), isBest(cellId)]"
        @drop="onDrop($event)"
        @dragover.prevent
        @dragenter.prevent>
        <Draught
          v-if="draught"
          :draughtId="draught.id"
          :playerId="draught.player_id"
          :startingPlayer="startingPlayer"
          :isQueen="draught.queen"
          draggable="true"
          @dragstart="onDragStart($event, draught, cellId)"
        />
      </div>
    </div>
    <div class="log">
      <textarea
        ref="log"
        id="log"
        name="log"
        v-model="getCurrentLog">
      </textarea>
    </div>
    <div class="buttons">
      <div style="display: grid">
        <span style="color: white">Hints:</span>
        <button
          class="button"
          :class="[ showHints ? 'button-secondary' : 'button-normal' ]"
          @click="toggleHints">
          {{ showHints ? 'ON' : 'OFF' }}
        </button>
        <br>
      </div>
      <div style="display: grid">
        <span style="color: white">Cheats:</span>
        <button
          class="button"
          :class="[ showCheats ? 'button-secondary' : 'button-normal' ]"
          @click="toggleCheats">
          {{ showCheats ? 'ON' : 'OFF' }}
        </button>
        <br>
      </div>
      <div style="display: grid">
        <span style="color: white">Back:</span>
        <button
          class="button button-normal"
          @click="$router.push({ path: '/' })">
          Back
        </button>
        <br>
      </div>
    </div>
  </div>
</template>

<script>
import axios from 'axios'
import Draught from '../components/Draught.vue'

// player ids
const AI = 0
const HUMAN = 1

// initialize AJAX requests to the server at port 5000
// set a 50 second timeout to allow waiting for search
const request = axios.create({
  baseURL: 'http://localhost:5000',
  timeout: 50000
})

export default {
  name: 'Board',
  components: {
    Draught,
  },
  props: {
    startingPlayer: {
      type: Number,
      default: HUMAN
    },
    difficulty: {
      type: Number,
      default: 1
    }
  },
  /**
   * store for the current board state, and other
   * variables which change what is rendered on the
   * game board, and store an array of messages for
   * rendering the log
   */
  data () {
    return {
      board: this.initializeBoard(),
      availableMoves: [],
      bestMove: {},
      winner: null,
      selected: null,
      showHints: true,
      showCheats: false,
      log: ['-- Pieces of Eight --']
    }
  },
  computed: {
    /**
     * apply a CSS class to highlight cells that
     * are the best move (when switched on)
     */
    isBest () {
      return (cellId) => {
        const isBest = this.bestMove.source == cellId || this.bestMove.destination == cellId
        return this.showCheats && isBest ? 'best' : ''
      }
    },
    /**
     * apply a CSS class to highlight cells that
     * are the valid moves for a single cell (when switched on)
     */
    isHint () {
      return (cellId) => {
        const isCurrent = this.selected == cellId
        const isNew = this.selected && this.isAvailableMove(cellId)
        return this.showHints && (isCurrent || isNew) ? 'hint' : ''
      }
    },
    getCurrentLog() {
      return this.log.join("\n")
    }
  },
  methods: {
    /**
     * callback for drag event of a human-controlled draught
     * cache the dragged item for on drop callback
     */
    onDragStart (event, draught, cellId) {
      if (draught.player_id == HUMAN) {
        this.selected = cellId
        event.dataTransfer.dropEffect = 'move'
        event.dataTransfer.effectAllowed = 'move'
        event.dataTransfer.setData('itemID', this.selected)
      } else {
        this.appendToLog(`-> Guybrush thats not your piece of eight`)
      }
    },
    /**
     * when a human drops a draught on a new location,
     * attempt to apply the move. errors will be rendered
     * and callbacks for AI activity won't execute unless
     * the move is valid, otherwise the AI follows up,
     * and finally new human moves are fetched
     */
    onDrop (event) {
      const destination = event.target.id
      const source = this.selected
      // always unset selected
      this.selected = null
      // if all values are set and the move is not the same cell, attempt to make the move
      if (destination && source && destination != source) {
        // first apply the human move
        this.postHumanMove(source, destination)
          // then apply the AI move
          .then(() => this.postAIMove())
          // then fetch latest human moves
          .then(() => this.getHumanMoves())
      }
    },
    /**
     * fetch all valid moves stored in component data
     * for a specific cell
     */
    getAvailableMoves (cellId) {
      const isValid = ([source, dests]) => parseInt(this.selected) == source && dests.includes(parseInt(cellId))
      return Object.entries(this.availableMoves).filter(isValid)
    },
    /**
     * check if a move is valid
     */
    isAvailableMove (cellId) {
      return this.getAvailableMoves(cellId).length
    },
    /**
     * set the position of a draught
     */
    setPosition (source, destination) {
      this.board[destination] = this.board[source]
      this.board[source] = null
    },
    /**
     * hints highlights areas of the board
     * that are valid moves
     */
    toggleHints () {
      if (this.winner) {
        return this.appendToLog(`-> The jig is up, you realise that right?`)
      }
      this.showHints = !this.showHints
      if (this.showHints) {
        this.appendToLog(`-> Do you really not know how to play this game?`)
      } else {
        this.appendToLog(`-> Stand aside Troll, I'm a mighty pirate`)
      }
    },
    /**
     * cheats provide the human with suggestions
     * using the minimax search algorithm
     * switch cheats on / off
     */
    toggleCheats () {
      if (this.winner) {
        return this.appendToLog(`-> The jig is up, you realise that right?`)
      }
      this.showCheats = !this.showCheats
      if (this.showCheats) {
        if (this.currentPlayer == HUMAN) {
          this.appendToLog(`-> Being a pirate is a pretty good excuse for most things`)
          this.fetchBestMove()
        }
      } else {
        this.appendToLog(`-> Good lad. No hornswaggling`)
      }
    },
    /**
     * creates a new game using the starting player
     * and the provided difficulty in props
     * then updates the board with the layout
     * provided by the back-end
     */
    createNewGame () {
      return new Promise((resolve, reject) => {
        request.post(`/game`, {
          player_id: this.startingPlayer,
          difficulty: this.difficulty
        }).then(({ data }) => {
          this.board = data.board
          return resolve()
        }).catch((err) => {
          const error = err.response.data.error
          this.appendToLog(`-> Guybrush ${error.message}`)
          return reject()
        })
      })
    },
    /**
     * fetches available moves and best moves
     * using decoupled asychronous requests
     */
    getHumanMoves () {
      if (this.winner) return
      this.currentPlayer = HUMAN
      this.fetchAvailableMoves()
      if (this.showCheats) this.fetchBestMove()
    },
    /**
     * API call applies the desired move using the
     * provided source and destination tiles
     * then clears any possible best move suggestions
     * and updates the entire board, if the player wins
     * then sets the game winner
     */
    postHumanMove (source, destination) {
      if (this.winner) return
      return new Promise((resolve, reject) => {
        request.post(`/move/${HUMAN}`, {
          source,
          destination
        }).then(({ data }) => {
          const [i, j] = this.cellIdToCoord(source)
          const [k, l] = this.cellIdToCoord(destination)
          // append the move to the log
          this.appendToLog(`-> Guybrush moves from (${i}, ${j}) to (${k}, ${l})`)
          // reset best move
          this.bestMove = {}
          // update the board
          this.board = data.board
          this.winner = data.winner
          if (this.winner != null) {
            this.appendToLog(`-> LeChuck walks the plank!`)
          }
          return resolve()
        }).catch((err) => {
          const error = err.response.data.error
          this.appendToLog(`-> Guybrush ${error.message}`)
          return reject()
        })
      })
    },
    /**
     * API calls first searches for the best AI move,
     * then applies that move to the state on the back-end
     * using a separate request, then updates the board
     * with each of then n possible moves in a move chain
     * then re-renders the entire board state with the final
     * to apply any possible queen transformations
     */
    postAIMove () {
      if (this.winner) return
      this.currentPlayer = AI
      return new Promise((resolve, reject) => {
        // append to log
        this.appendToLog(`-> LeChuck is thinking...`)
        // fetch AI moves
        request.get(`/moves/${AI}/search`).then(({ data }) => {
          if (!data.move) {
            this.winner = HUMAN
            this.appendToLog(`-> LeChuck walks the plank!`)
            return resolve()
          }
          // unpack the move
          let { source, destination } = data.move
          this.appendToLog(`-> LeChuck considered ${data.evaluations} different outcomes`)
          // apply the move on the back-end
          request.post(`/move/${AI}`, data.move).then(({ data }) => {
            const [i, j] = this.cellIdToCoord(source)
            const [k, l] = this.cellIdToCoord(destination)
            // append move details to log
            this.appendToLog(`-> LeChuck moves from (${i}, ${j}) to (${k}, ${l})`)
            // apply the move in the UI
            // stagger each move with a timeout
            this.applyMoveChain(data.moves, () => {
              // fetch the latest board state
              this.board = data.board
              this.winner = data.winner
              if (this.winner != null) {
                this.appendToLog(`-> Guybrush walks the plank!`)
              }
              return resolve()
            })
          }).catch(handleError)
        }).catch(handleError)

        function handleError (err) {
          const error = err.response.data.error
          this.appendToLog(`-> LeChuck ${error.message}`)
          reject()
        }
      })
    },
    /**
     * API call fetches all possible moves for the human
     * and stores the results in component data
     */
    fetchAvailableMoves () {
      // fetch and cache the available moves for the human
      return new Promise((resolve, reject) => {
        request.get(`/moves/${HUMAN}`).then(({ data }) => {
          // if the player can't move, AI wins
          if (!Object.keys(data.moves).length) {
            this.winner = AI
            return resolve()
          }
          this.availableMoves = data.moves
          resolve()
        }).catch((err) => {
          const error = err.response.data.error
          this.appendToLog(`-> Guybrush ${error.message}`)
          reject()
        })
      })
    },
    /**
     * API call searches for the best move for the human
     * and stores the result in component data
     */
    fetchBestMove () {
      return new Promise((resolve, reject) => {
        // append to the log
        this.appendToLog(`-> Asking the Sword Master for recommendations...`)
        // search for the best moves for the human
        request.get(`/moves/${HUMAN}/search`, { params: {
          max_depth: 10,
          max_evaluations: 500000
        }}).then(({ data }) => {
          // append to log
          const [i, j] = this.cellIdToCoord(data.move.source)
          const [k, l] = this.cellIdToCoord(data.move.destination)
          this.appendToLog(`-> The Sword Master considered ${data.evaluations} different outcomes`)
          this.appendToLog(`-> The Sword Master recommends moving from (${i}, ${j}) to (${k}, ${l})`)
          // cache the best move
          this.bestMove = data.move
          resolve()
        }).catch((err) => {
          const error = err.response.data.error
          this.appendToLog(`-> Guybrush ${error.message}`)
          reject()
        })
      })
    },
    /**
     * applies a series of moves with a 500ms timeout
     * between each move, used for nested capture moves
     */
    applyMoveChain (moves, callback) {
      const self = this
      next(0)

      function next (i) {
        const move = moves[i]
        if (!move) return callback()
        const [source, destination, captured] = move
        if (captured) self.board[captured] = null
        self.setPosition(source, destination)
        setTimeout(() => next(i + 1), 500)
      }
    },
    /**
     * sets the default game state on component load
     * before the back-end overwrites
     */
    initializeBoard () {
      const board = {}
      for (let cellId = 0; cellId < 64; cellId++) {
        board[cellId] = null
        const [i, j] = this.cellIdToCoord(cellId)
        if ((i % 2 == 0 && j % 2 == 1) || (i % 2 == 1 && j % 2 == 0)) {
          if (cellId < 24) {
            board[cellId] = { playerId: AI }
          } else if (cellId > 39) {
            board[cellId] = { playerId: HUMAN }
          }
        }
      }
      return board
    },
    /**
     * transforms an integer to a coordinate
     */
    cellIdToCoord(cellId) {
      const i = parseInt(cellId / 8)
      const j = cellId % 8
      return [i, j]
    },
    /**
     * transforms an (i, j) coordinate to an integer
     */
    coordToCellId(i, j) {
      return (i * 8) + j
    },
    /**
     * sets fixed tile colours
     */
    defaultTileColour (cellId) {
      const [i, j] = this.cellIdToCoord(cellId)
      if (i % 2 == 0 && j % 2 == 0) {
        return 'white'
      } else if (i % 2 == 0 && j % 2 == 1) {
        return 'black'
      } else if (i % 2 == 1 && j % 2 == 0) {
        return 'black'
      } else if (i % 2 == 1 && j % 2 == 1) {
        return 'white'
      }
    },
    /**
     * appends to the text area
     * capturing game state text annotations
     */
    appendToLog (message) {
      this.log.push(message)
      const el = this.$refs['log']
      setTimeout(() => el.scrollTop = el.scrollHeight, 100)
    },
    /**
     * initialize a new game
     */
    initialize () {
      this.board = this.initializeBoard()
      // create a new game based on the starting player
      this.createNewGame().then(() => {
        // if the Ai is starting, make their move, then get human moves
        if (this.startingPlayer == AI) {
          this.postAIMove().then(() => this.getHumanMoves())
        // otherwise the player started, so just get human moves
        } else {
          this.getHumanMoves()
        }
      })
    }
  },
  /**
   * called when component is first rendered
   * creates a new game instance, then applies
   * the relevant move (HUMAN or AI)
   */
  mounted() {
    this.initialize()
  }
}
</script>

<style scoped>
.board-container {
  width: 480px;
  margin: auto;
}
.board {
  width: 480px;
  height: 480px;
  margin: 20px 0 20px 0;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
}
.board + .board {
  border: 4px solid white;
}
.tile {
  float: left;
  width: 60px;
  height: 60px;
  text-align: center;
  display: table-cell;
  vertical-align: middle;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
}
.black {
  background-color: #60bbd2;
}
.white {
  background-color: #b2dde6;
}
.hint {
  background-color: #AE76A6;
}
.tile + .best {
  border: 4px solid #04F06A;
}
button {
  float: right;
}
textarea {
  font-size: 8pt;
  width: 480px;
  height: 20rem;
}
</style>
```

---

```html
<!-- ui/components/draught.vue -->

<template>
  <div
    v-if="playerId != undefined"
    class="draught circle"
    @click="onClick"
    @draggable="true"
    @dragstart="onDragStart"
    :class="[ playerColour, queenBorder ]">
  </div>
</template>

<script>
export default {
  name: 'Draught',
  props: {
    location: {
      type: Number
    },
    playerId: {
      type: Number
    },
    startingPlayer: {
      type: Number
    },
    draughtId: {
      type: Number
    },
    isQueen: {
      type: Boolean
    }
  },
  computed: {
    getCoordinate (n) {
      return [parseInt(n / 8), n % 8]
    },
    playerColour () {
      return this.playerId == this.startingPlayer ? 'dark' : 'light'
    },
    queenBorder () {
      return this.isQueen ? 'queen' : ''
    }
  },
  methods: {
    onClick(event) {
      this.$emit('click', event)
    },
    onDragStart(event) {
      this.$emit('dragstart', event)
    }
  }
}
</script>

<style scoped>
.circle {
  cursor: pointer;
  position: relative;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  border: 2px solid black;
  width: 45px;
  height: 45px;
  border-radius: 40px;
}
.light{
  background-color: #FFF0FF;
}
.dark {
  background-color: #373F51;
}
.queen {
  border: 2px solid #04F06A;
}
</style>
```

### Server

I have included the referenced classes and code. The remaining code is contained in the provided zip archive, including local types, game constants and HTTP error handling.

```python
# server/lib/constants.py

PLAYER_1 = 0
PLAYER_2 = 1
PAWN = 1
QUEEN = 2
```

---

```python
# server/lib/local_types.py

from typing import Iterable, Optional, Tuple, Dict
# A player is a list of draught locations and their values
Player = Dict[int, int]
# State is a tuple of the two players
State = Tuple[Player, Player]
```

---

```python
# server/lib/move.py

from typing import Optional
from lib.local_types import State
from lib.constants import PLAYER_1, PLAYER_2, QUEEN, PAWN

class Move:
    """
    Move is a recursive data structure. When moving draughts pieces,
    a move may be singular, or have multiple steps when captures are
    made, and n captures can be completed in a single turn. These can
    fan out in a tree structure, as there may be multiple pathways
    available following a single capture. As such each move contains
    a reference to its parent, such that the leaf nodes can be traced
    back to form a chain of moves. A move instance caches the values
    of the pieces that are captured, such that moves can be easily
    reversed or undone. Moves also contain the logic that applies the
    promotion of a pawn to a queen, either through reaching the rear line
    or through regicide (capturing another queen). When a move is applied
    it makes changes to the provided state representations for each player
    """
    def __str__(self):
        return str((self.source, self.destination, self.captured))

    def __init__(self,
                 source: int,
                 destination: int,
                 captured: Optional[int] = None,
                 source_value: int = PAWN,
                 destination_value: int = PAWN,
                 captured_value: Optional[int] = None,
                 parent: Optional["Move"] = None):
        self.source = source
        self.destination = destination
        self.captured = captured
        self.source_value = source_value
        self.destination_value = destination_value
        self.captured_value = captured_value
        self.parent = parent

    def to_json(self):
        """
        return the root move source and
        the current move's destination
        as a dict for easy json conversion
        """
        return {
            "source": self.get_source(),
            "destination": self.destination
        }

    def get_source(self):
        """
        fetch the root move of a move
        """
        source = None
        move = self
        while move:
            source = move.source
            move = move.parent
        return source

    def get_source_and_destinations(self):
        """
        compile a tuple of the move source,
        and a sequential list of destinations
        """
        destinations = []
        source = None
        move = self
        while move:
            source = move.source
            destinations.append(move.destination)
            move = move.parent
        return source, destinations

    def limit_by_destination(self, destination):
        """
        return the move from a chain, limited by the destination
        """
        move = self
        while move:
            if move.destination == destination:
                return move
            move = move.parent

    def apply(self,
              state: State,
              player_id: int):
        """
        apply a single move to the current state
        caching any captured values for easy reversal
        """
        current_player, other_player = state
        self.source_value = current_player[self.source]
        # move to destination, crown queen if applicable
        destination_value = self.source_value
        # delete any captured pieces from other player
        if self.captured:
            self.captured_value = other_player[self.captured]
            del other_player[self.captured]
        # delete from source
        del current_player[self.source]
        # we become a queen whenever player 1 or player 2 hits the queens row
        # or through regicide, whenever we capture an opponent queen
        if ((player_id == PLAYER_1 and 56 <= self.destination <= 63) or
                (player_id == PLAYER_2 and 0 <= self.destination <= 7) or
                self.captured_value == QUEEN):
            destination_value = QUEEN
        self.destination_value = destination_value
        current_player[self.destination] = self.destination_value

    def apply_chain(self,
                    state: State,
                    player_id: int):
        """
        apply an entire move chain to the current state
        caching any captured values for easy reversal
        """
        current_player, other_player = state
        # delete any captured pieces from other player
        move: Optional[Move] = self
        # we need to figure out the source
        source = None
        crowned = False
        while move:
            # remove captures until we find the highest source
            if move.captured:
                move.captured_value = other_player[move.captured]
                del other_player[move.captured]
            # move to destination, crown queen if applicable including regicide
            if ((player_id == PLAYER_1 and 56 <= move.destination <= 63) or
                    (player_id == PLAYER_2 and 0 <= move.destination <= 7) or
                    move.captured_value == QUEEN):
                crowned = True
            source = move.source
            move = move.parent
        # delete from source
        destination_value = QUEEN if crowned else current_player[source]
        del current_player[source]
        current_player[self.destination] = destination_value

    def undo(self,
             state: State,
             player_id: int):
        """
        apply the reverse of a move to the current state
        """
        current_player, other_player = state
        # recover any captured pieces
        if self.captured and self.captured_value:
            other_player[self.captured] = self.captured_value
        # delete from destination
        del current_player[self.destination]
        # move back to source
        current_player[self.source] = self.source_value

    def undo_chain(self,
                   state: State,
                   player_id: int):
        """
        apply the reverse of a chain of moves to the current state
        """
        current_player, other_player = state
        # recover any captured pieces
        move: Optional[Move] = self
        # figure out the source and original value
        source = None
        source_value = None
        while move:
            # insert captures back in along the way
            if move.captured and move.captured_value:
                other_player[move.captured] = move.captured_value
            source = move.source
            source_value = move.source_value
            move = move.parent
        # delete from destination
        del current_player[self.destination]
        # move back to source
        current_player[source] = source_value

    def chain(self):
        """compile an entire move chain as a list"""
        return self.__get_chain__(self, [self])

    def __get_chain__(self, move, moves = []):
        """recurse to return a chain of moves as a list"""
        if move.parent == None:
            moves.reverse()
            return moves
        moves.append(move.parent)
        return self.__get_chain__(move.parent, moves)
```

---

```python
# server/lib/game.py

import code
import copy
import random
from typing import List, Iterable, Optional, Dict, Tuple
from lib.local_types import State, Player
from lib.constants import PLAYER_1, PLAYER_2, QUEEN, PAWN
from lib.move import Move

class Game:
    """
    Game contains all core game logic. This includes:

    - our game state representation is composed of two dictionaries storing each player's state
      the key is an integer corresponding to a valid tile on the game board,
      the value is the value of that piece, either a pawn or a queen.

    - our successor function is composed of three distinct functions,
      'neighbours' fetches all possible neighbouring tiles
      'captures' fetches move chains that correspond to all possible capture pathways
      'available_moves' returns all possible moves, including (and restricted by) captures

    - our goal check function which checks to see if a player has won the game is contained within 'update'
    """
    def __init__(self,
                 starting: int = PLAYER_2,
                 difficulty: int = 0) -> None:
        assert 0 <= starting <= 1, 'starting player must have an id of 0 or 1'
        self.turn = starting
        self.difficulty = difficulty
        self.player_1: Player = {}
        self.player_2: Player = {}
        # generate starting state
        self.__initial_state__()
        # store in a list to fetch easily
        self.players: List[Player] = [self.player_1,
                                      self.player_2]
        # the winner is unassigned
        self.winner: Optional[int] = None

    def state_to_json(self):
        """
        returns a dictionary representation of the current board state
        """
        state = {}
        for n in range(64):
            state[n] = None
            if value := self.player_1.get(n):
                state[n] = { "player_id": PLAYER_1, "queen": value == QUEEN }
            elif value := self.player_2.get(n):
                state[n] = { "player_id": PLAYER_2, "queen": value == QUEEN }
        return state

    def available_moves_to_json(self,
                                current_state: State,
                                player_id: int):
        """
        returns a dictionary representation of the available moves for a given player and state
        """
        json: Dict[int, List] = {}
        for move in self.available_moves(current_state, player_id):
            source, destinations = move.get_source_and_destinations()
            if json.get(source):
                json[source].extend(destinations)
            else:
                json[source] = destinations
        return json

    def current_player_id(self) -> int:
        """
        current player id is either 0 or 1
        """
        return self.turn % 2

    def current_player(self) -> Player:
        """
        the player state dict for the current player
        """
        return self.players[self.current_player_id()]

    def other_player_id(self) -> int:
        """
        inverse of current player id
        """
        return abs(1 - self.current_player_id())

    def other_player(self) -> Player:
        """
        the player state dict for the other player / opponent
        """
        return self.players[self.other_player_id()]

    def current_state(self) -> State:
        """
        current state is a tuple where the current player is the first
        """
        if self.current_player_id() == PLAYER_1:
            return (self.player_1, self.player_2)
        else:
            return (self.player_2, self.player_1)

    def clone_state(self) -> State:
        """
        deep copy the current state for search
        """
        return copy.deepcopy(self.current_state())

    def select_move(self,
                    moves: List[Move],
                    is_random: bool = True):
        """
        either select randomly or take the first moves from a list of moves
        """
        return moves[random.randint(0, len(moves) - 1)] if is_random else moves[0]

    def update(self):
        """
        perform a goal check, i.e. the opponent has no remaining pieces, then increment the turn
        """
        if not len(self.player_1):
            self.winner = PLAYER_2
        elif not len(self.player_2):
            self.winner = PLAYER_1
        self.turn += 1

    def neighbours(self,
                   source: int,
                   player_id: int,
                   is_queen: bool = False) -> Iterable[int]:
        """
        neighbouring squares are defined according to our state representation.
        The board is defined as indices between 0 and (8 x 8) - 1 = 63 (note that alternating
        odd and even squares cannot be occupied). neighbours function takes a source integer
        between 0 and 63 and returns all possible neighbours irrespective of whether
        neighbouring squares are occupied or not. Player 1 moves down the board, so all
        neighbouring squares must be later in the sequence, we add 7 or 9 and ensure that
        only the next row of 8 numbers is valid. Player 2 moves up the board, so all neighbouring
        squares must be earlier in the sequence, we subtract 7 or 9 and ensure the previous
        row of 8 numbers is valid. queens can move up or down, so if the piece at the source
        is a queen, we treat as if it is both players.
        """
        # player 1 can only move up the board, all queens can move up
        i = int(source / 8)
        if player_id == PLAYER_1 or is_queen:
            # upper bound of the board is 63
            # first term calculates the relevant square, second term ensures
            # it remains on a neighbouring row
            if (left := source + 7) <= 63 and int(left / 8) - i == 1:
                yield left
            if (right := source + 9) <= 63 and int(right / 8) - i == 1:
                yield right
        # player 2 can only move down the board, all queens can move down
        if player_id == PLAYER_2 or is_queen:
            # lower bound of the board is 0
            if (right := source - 7) >= 0 and int(right / 8) - i == -1:
                yield right
            if (left := source - 9) >= 0 and int(left / 8) - i == -1:
                yield left

    def captures(self,
                 move: Move,
                 state: State,
                 player_id: int,
                 moves: List[Move] = []):
        """
        starting at the move's destination (the projected current location for the piece)
        recursively discover capture moves, collecting only the leaf nodes (moves are a nested
        data structure and can easily be reconstituted to form a full move path)
        """
        # fetch state
        current_player_state, other_player_state = state
        source, value = move.destination, move.destination_value
        capturing = False
        is_queen = (value == QUEEN)
        # fetch all possible neighbouring squares
        for destination in self.neighbours(source, player_id, is_queen):
            # if a neighbour is occupied by an opponent, check if the next square is empty
            if destination in other_player_state.keys():
                # find next position along the same axis
                i, j = int(source / 8), source % 8
                k, l = int(destination / 8), destination % 8
                m, n = k + (k - i), l + (l - j)
                captured = destination
                destination = m * 8 + n
                # check the new destination is within the bounds
                if ((0 <= destination <= 63) and
                        # and the original destination is not a border square
                        (0 < l < 7) and
                        # and its unoccupied
                        destination not in current_player_state and
                        destination not in other_player_state):
                    capturing = True
                    # build and yield a move
                    next_move = Move(source=source,
                                     destination=destination,
                                     captured=captured,
                                     parent=move)
                    # temporarily apply the move
                    next_move.apply(state, player_id)
                    # recurse until maximum depth
                    self.captures(next_move, state, player_id, moves)
                    # undo the move
                    next_move.undo(state, player_id)
                    # yield the original capture move
        if not capturing:
            # we've hit maximum depth, we can
            # do no more, append move to leaf nodes
            moves.append(move)
        return moves

    def available_moves(self,
                        state: State,
                        player_id: int) -> Iterable[Move]:
        """
        the available moves or successor function for the current state takes into consideration all current
        pieces of a given player, and searches for possible locations to move according to what constitutes a
        valid transition. Transitions / neighbouring squares are valid when they are unoccupied.
        If a square is occupied by an opponent, and the square beyond it along the same axis is unoccupied,
        this too is a valid move, and a capture is available. When a capture is available, we then only
        consider other captures as valid moves, forcing players to perform capture moves.
        """
        # fetch state
        current_player_state, other_player_state = state
        # store all moves
        moves: List[Move] = []
        # store capture moves separately, if this is populated we return only capture moves
        capturing: List[Move] = []
        # for every piece that player has
        for source in list(current_player_state.keys()):
            # check if its a queen
            is_queen = (current_player_state[source] == QUEEN)
            # fetch all neighbour squares
            for destination in self.neighbours(source, player_id, is_queen):
                # check if they're valid
                # is this occupied by our own piece?
                if destination in current_player_state.keys():
                    pass
                # is an opponent there?
                elif destination in other_player_state.keys():
                    # find next position along the same axis
                    i, j = int(source / 8), source % 8
                    k, l = int(destination / 8), destination % 8
                    m, n = k + (k - i), l + (l - j)
                    captured = destination
                    destination = m * 8 + n
                    # check the new destination is within the bounds
                    if ((0 <= destination <= 63) and
                            # and the original destination is not a border square
                            (0 < l < 7) and
                            # and its unoccupied
                            destination not in current_player_state and
                            destination not in other_player_state):
                        # build and yield a root node move
                        move = Move(source=source,
                                    destination=destination,
                                    captured=captured)
                        # temporarily apply the move
                        move.apply(state, player_id)
                        # recurse until there are no more capture pathways
                        capture_move_chains = self.captures(move, state, player_id, [])
                        # merge into list of capturing moves (leaf nodes)
                        capturing.extend(capture_move_chains)
                        # undo the move
                        move.undo(state, player_id)
                # non-capture moves only valid when there are no captures (forced capture)
                elif not len(capturing):
                    move = Move(source, destination)
                    moves.append(move)
        # return capturing only if there are any, otherwise return all possible moves
        return capturing if len(capturing) else moves

    def __initial_state__(self):
        """
        build initial state based on 8x8 starting grid
        alternating odd and even rows and columns
        """
        for n in range(64):
            # transform into a discrete co-ordinate
            i = int(n / 8)
            j = n % 8
            # alternating odd / even
            if ((i % 2 == 0 and j % 2 == 1) or
                    (i % 2 == 1 and j % 2 == 0)):
                # player 1 on the first three rows
                if n < 24:
                    self.player_1[n] = 1
                # player 2 the last three rows
                elif n > 39:
                    self.player_2[n] = 1
```

---

```python
# server/lib/search.py

import code
import random
from typing import Iterable, List, Optional, Tuple
from lib.game import Game
from lib.local_types import State
from lib.move import Move

Evaluation = Tuple[Move, int]

INF = float('inf')

class Search:
    """
    Search contains the code for performing search in an adversarial context using
    the minimax algorithm with alpha-beta pruning and a heuristic to compute the
    static evaluations at the search's leaf nodes and determine the score of a certain
    search pathway
    """
    def __init__(self,
                 game: Game,
                 max_depth: Optional[int] = None,
                 max_evaluations: Optional[int] = None):
        self.game = game
        self.max_depth = max_depth or self.__max_depth__()
        self.max_static_evaluations = max_evaluations or self.__max_evaluations__()
        self.static_evaluation_count = 0
        self.dynamic_evaluation_count = 0
        self.evaluations: List[Evaluation] = []

    def __max_depth__(self):
        """maximum depth according to game difficulty"""
        return {
            1: 1,
            2: 5,
            3: 10
        }[self.game.difficulty]

    def __max_evaluations__(self):
        """
        maximum number of evaluations according to the game difficulty
        """
        return {
            1: 250,
            2: 10_000,
            3: 500_000
        }[self.game.difficulty]

    def evaluate(self) -> Optional[Move]:
        """
        evaluate calls the minimax algorithm using alpha-beta pruning
        to determine the best move for the current player given the current state
        """
        state = self.game.clone_state()
        self.minimax_alpha_beta(state, self.game.current_player_id())
        # if there are numerous pathways to a best score
        # then return all of them
        best_move = None
        best_score = -INF
        for move, score in self.evaluations:
            if best_score < score:
                best_score = score
                best_move = move
        # reset the evaluations
        self.evaluations.clear()
        # and return the best moves
        return best_move

    def static_evaluation(self,
                          state: State,
                          player_id: int,
                          depth) -> Optional[int]:
        """our static evaluation results are determined by our heuristic function h(n)"""
        return self.heuristic(state,
                              player_id,
                              depth)

    def heuristic(self,
                  state: State,
                  player_id: int,
                  depth: int) -> Optional[int]:
        """
        both players seek to increase the difference between the total value its
        their own pieces and their opponents, incentivising captures and crowning
        """
        current_player_state, other_player_state = state
        # first check if we've hit the maximum depth
        if depth == self.max_depth and player_id == self.game.current_player_id():
            # current i.e. maximising player seeks to increase the difference between
            # the total value of pieces it has, and the total value of pieces its opponent has
            return sum(current_player_state.values()) - sum(other_player_state.values())
            # return len(current_player_state) - len(other_player_state)
        elif depth == self.max_depth and player_id == self.game.other_player_id():
            # other i.e. minimising player seeks to decrease the difference between
            # the total value of pieces it has and the total value of pieces its opponent has
            return -(sum(current_player_state.values()) - sum(other_player_state.values()))
        # if none of these applies, keep searching
        return None

    def minimax_alpha_beta(self,
                           state: State,
                           player_id: int,
                           alpha: float = -INF,
                           beta: float = INF,
                           depth: int = 0):
        """
        minimax is a search algorithm for an adversarial context, seeking to find 
        the best move to a certain depth. The current player seeks to maximise the
        utility of a given move, while the other player (opponent) will seek to
        minimise the other player's result. alpha and beta prune the search so we 
        only expand nodes that will give good outcomes for the maximising player
        """
        # return heuristic value if evaluating at the leaf nodes
        evaluation = self.static_evaluation(state, player_id, depth)
        if (evaluation != None):
            self.static_evaluation_count += 1
            return evaluation
        # best score defaults to +/- 12, the worst possible outcome for min/max
        best_score = -12 if player_id == self.game.current_player_id() else 12
        # successor function fetches all valid moves for the player in question, given current state
        for move in self.game.available_moves(state, player_id):
            # regardless of whether we're maximising or minimising, the move(s) are made
            move.apply_chain(state, player_id)
            # setup state for deepening
            next_state = tuple(reversed(state))
            # maximising player
            if player_id == self.game.current_player_id():
                # next recursion minimises for other player, recurse until static evaluation
                current_score = self.minimax_alpha_beta(next_state,
                                                        self.game.other_player_id(),
                                                        alpha,
                                                        beta,
                                                        depth=depth + 1)
                # perform dynamic evaluation
                best_score = max(best_score, current_score)
                alpha = max(current_score, alpha)
                self.dynamic_evaluation_count += 1
                # if first depth layer, store evaluations to inform final decision
                if depth == 0:
                    self.evaluations.append((move, current_score))
            # minimising player
            elif player_id == self.game.other_player_id():
                # next recursion maximises for current player, recurse until static eval
                current_score = self.minimax_alpha_beta(next_state,
                                                        self.game.current_player_id(),
                                                        alpha,
                                                        beta,
                                                        depth=depth + 1)
                # perform dynamic evaluation
                best_score = min(best_score, current_score)
                beta = min(current_score, beta)
                self.dynamic_evaluation_count += 1
            # reverse the move(s)
            move.undo_chain(state, player_id)
            # don't explore unnecessary pathways (pruning) / return early if max evaluations are reached
            if ((alpha >= beta) or
                    (self.static_evaluation_count == self.max_static_evaluations)):
                break
        # return the best score
        return best_score
```

---

```python
# server/test/test_game.py

import code
from lib.game import Game
from lib.constants import PLAYER_1, PLAYER_2, QUEEN, PAWN

def test_player_1_neighbours():
    game = Game()

    assert list(game.neighbours(44, PLAYER_1)) == [51, 53]

    assert list(game.neighbours(0, PLAYER_1)) == [9], \
        'player 1 draughts in nearside corner (i = even) have only one diagonal neighbour'

    assert list(game.neighbours(5, PLAYER_1)) == [12, 14], \
        'player 1 draughts in nearside corner (i = odd) have two diagonal neighbours'

    assert list(game.neighbours(9, PLAYER_1)) == [16, 18], \
        'player 1 draughts in center have two diagonal neighbours'

    assert list(game.neighbours(9, PLAYER_1, is_queen=True)) == [16, 18, 2, 0], \
        'player 1 queens in center have four diagonal neighbours'

    assert list(game.neighbours(15, PLAYER_1)) == [22], \
        'player 1 draughts on edge (i = odd) have one diagonal neighbour'

    assert list(game.neighbours(47, PLAYER_1)) == [54], \
        'player 1 draughts on edge (i = even) have one diagonal neighbour'

    assert list(game.neighbours(47, PLAYER_1, is_queen=True)) == [54, 38], \
        'player 1 queens on edge have two diagonal neighbours'

    assert list(game.neighbours(62, PLAYER_1)) == [], \
        'player 1 draughts in farside corner (i = odd) have no neighbours'

    assert list(game.neighbours(62, PLAYER_1, is_queen=True)) == [55, 53], \
        'player 1 queens in farside corner (i = odd) have one neighbour'

    assert list(game.neighbours(56, PLAYER_1)) == [], \
        'player 1 draughts in farside corner (i = even) have no neighbours'

    assert list(game.neighbours(56, PLAYER_1, is_queen=True)) == [49], \
        'player 1 queens in farside corner (i = even) have one neighbour'

    assert list(game.neighbours(59, PLAYER_1)) == [], \
        'player 1 draughts on farside in center have no neighbours'

    assert list(game.neighbours(60, PLAYER_1, is_queen=True)) == [53, 51], \
        'player 1 queens on farside in center have two neighbours'

def test_player_2_neighbours():
    game = Game()

    assert list(game.neighbours(60, PLAYER_2)) == [53, 51], \
        'player 2 draughts on nearside in center have two neighbours'

    assert list(game.neighbours(56, PLAYER_2)) == [49], \
        'player 2 draughts in nearside corner (i = odd) have two diagonal neighbours'

    assert list(game.neighbours(62, PLAYER_2)) == [55, 53], \
        'player 2 draughts in nearside corner (i = even) have only one diagonal neighbour'

    assert list(game.neighbours(53, PLAYER_2)) == [46, 44], \
        'player 2 draughts in center have two diagonal neighbours'

    assert list(game.neighbours(53, PLAYER_2, is_queen=True)) == [60, 62, 46, 44], \
        'player 2 queens in center have four diagonal neighbours'

    assert list(game.neighbours(47, PLAYER_2)) == [38], \
        'player 2 draughts on edge (i = odd) have one diagonal neighbour'

    assert list(game.neighbours(40, PLAYER_2)) == [33], \
        'player 2 draughts on edge (i = even) have one diagonal neighbour'

    assert list(game.neighbours(40, PLAYER_2, is_queen=True)) == [49, 33], \
        'player 2 queens on edge have two diagonal neighbours'

    assert list(game.neighbours(0, PLAYER_2)) == [], \
        'player 2 draughts in farside corner (i = odd) have no neighbours'

    assert list(game.neighbours(0, PLAYER_2, is_queen=True)) == [9], \
        'player 2 queens in farside corner (i = odd) have one neighbour'

    assert list(game.neighbours(6, PLAYER_2)) == [], \
        'player 2 draughts in farside corner (i = even) have no neighbours'

    assert list(game.neighbours(5, PLAYER_2, is_queen=True)) == [12, 14], \
        'player 2 queens in farside corner (i = even) have two neighbours'

    assert list(game.neighbours(1, PLAYER_2)) == [], \
        'player 2 draughts on farside in center have no neighbours'

    assert list(game.neighbours(1, PLAYER_2, is_queen=True)) == [8, 10], \
        'player 2 queens on farside in center have two neighbours'

def test_player_1_available_moves():
    game = Game(starting=0)
    # at the beginning of the game, available moves for player one are:
    moves = [(move.source, move.destination, move.captured)
             for move in game.available_moves(game.clone_state(), game.current_player_id())]
    assert moves == [(17, 24, None),
                     (17, 26, None),
                     (19, 26, None),
                     (19, 28, None),
                     (21, 28, None),
                     (21, 30, None),
                     (23, 30, None)], 'returns a list of all valid player 1 successor moves'

    game.player_1[28] = game.player_1[19]
    del game.player_1[19]

    moves = [(move.source, move.destination, move.captured)
             for move in game.available_moves(game.clone_state(), game.current_player_id())]
    assert moves == [(10, 19, None),
                     (12, 19, None),
                     (17, 24, None),
                     (17, 26, None),
                     (21, 30, None),
                     (23, 30, None),
                     (28, 35, None),
                     (28, 37, None)], 'returns a list of all valid player 1 successor moves'

def test_player_2_available_moves():
    game = Game(starting=1)
    # at the beginning of the game, available moves for player two are:
    moves = [(move.source, move.destination, move.captured)
             for move in game.available_moves(game.clone_state(), game.current_player_id())]
    assert  moves == [(40, 33, None),
                      (42, 35, None),
                      (42, 33, None),
                      (44, 37, None),
                      (44, 35, None),
                      (46, 39, None),
                      (46, 37, None)], 'returns a list of all valid player 2 successor moves'

    game.player_2[33] = game.player_2[42]
    del game.player_2[42]

    moves = [(move.source, move.destination, move.captured)
             for move in game.available_moves(game.clone_state(), game.current_player_id())]
    assert  moves == [(44, 37, None),
                      (44, 35, None),
                      (46, 39, None),
                      (46, 37, None),
                      (49, 42, None),
                      (51, 42, None),
                      (33, 26, None),
                      (33, 24, None)], 'returns a list of all valid player 2 successor moves'

def test_forced_capture_single_player_1():
    game = Game(starting=0)
    # move and increment turn
    game.player_1[28] = game.player_1[19]
    del game.player_1[19]
    # move a piece to a capturable location
    game.player_2[35] = game.player_2[42]
    del game.player_2[42]
    # only one available move, a forced capture
    moves = [(move.source, move.destination, move.captured)
             for move in game.available_moves(game.clone_state(), game.current_player_id())]
    assert moves == [(28, 42, 35)], 'returns a single capture successor move'

def test_forced_capture_choice_player_1():
    game = Game(starting=0)
    # move and increment turn
    game.player_1[28] = game.player_1[19]
    del game.player_1[19]
    # move pieces to capturable locations
    game.player_2[35] = game.player_2[42]
    del game.player_2[42]
    game.player_2[37] = game.player_2[46]
    del game.player_2[46]
    # two available moves, a choice of forced captures
    moves = [(move.source, move.destination, move.captured)
             for move in game.available_moves(game.clone_state(), game.current_player_id())]
    assert moves == [(28, 42, 35),
                     (28, 46, 37)], 'returns a list of capture successor moves'

def test_forced_capture_choice_player_2():
    game = Game(starting=1)
    # move and increment turn
    game.player_1[28] = game.player_1[19]
    del game.player_1[19]
    # move pieces to capturable locations
    game.player_2[35] = game.player_2[42]
    del game.player_2[42]
    game.player_2[37] = game.player_2[46]
    del game.player_2[46]
    game.player_1[42] = game.player_1[28]
    del game.player_1[28]
    del game.player_2[35]
    moves = [(move.source, move.destination, move.captured)
             for move in game.available_moves(game.clone_state(), game.current_player_id())]
    assert moves == [(49, 35, 42), (51, 33, 42)], 'returns a list of unique capture successor moves'

def test_forced_capture_chain():
    game = Game(starting=0)
    # move player 1 into position
    game.player_1[28] = game.player_1[19]
    del game.player_1[19]
    # move player 2 into position and delete piece on backrow
    game.player_2[35] = game.player_2[42]
    del game.player_2[42]
    del game.player_2[60]
    moves = game.available_moves(game.clone_state(), game.current_player_id())
    # fetch the only available move
    move = moves[0]
    # this move has a chain, there are two hops we can make
    chain = [(move.source, move.destination, move.captured) for move in move.chain()]
    assert chain == [(28, 42, 35), (42, 60, 51)], 'returns a chain of capture successor moves'

def test_queen_capture():
    players = [{ 7: PAWN, 19: PAWN, 21: PAWN, 23: PAWN },
               { 10: QUEEN, 46: PAWN, 51: PAWN, 55: PAWN, 60: PAWN, 62: PAWN }]
    game = Game(starting=1)
    game.player_1 = players[0]
    game.player_2 = players[1]
    game.players = players
    moves = game.available_moves(game.clone_state(), game.current_player_id())
    move = moves[0]
    chain = [(move.source, move.destination, move.captured) for move in move.chain()]
    assert chain == [(10, 28, 19), (28, 14, 21)], 'returns a chain of queen capture successor moves'

def test_queen_double_capture_reverse_direction():
    players = [{ 24: PAWN, 49: QUEEN },
               { 5: QUEEN, 26: QUEEN, 37: QUEEN, 39: PAWN, 42: PAWN, 44: PAWN, 46: PAWN, 53: PAWN, 55: PAWN }]
    game = Game(starting=0)
    game.player_1 = players[0]
    game.player_2 = players[1]
    game.players = players
    moves = game.available_moves(game.clone_state(), game.current_player_id())
    move = moves[0]
    chain = [(move.source, move.destination, move.captured) for move in move.chain()]
    assert chain == [(49, 35, 42), (35, 17, 26)], 'returns a chain of queen capture successor moves'
```

---

```python
# server/lib/http_error.py

class HTTPError(Exception):
    def __init__(self, message, status_code):
        self.message = message
        self.status_code = status_code

    def __str__(self):
        if self.status_code == 500:
            return f"Internal Server Error: {self.message}"
        elif self.status_code == 400:
            return f"Bad Request: {self.message}"
        elif self.status_code == 404:
            return f"Not Found: {self.message}"
```

---

```python
# server/app.py

import code
from flask import Flask, jsonify, request
from flask import render_template
from flask_cors import CORS
from typing import Optional, Dict, List
from lib.game import Game
from lib.search import Search
from lib.constants import PAWN, QUEEN
from lib.local_types import State
from lib.http_error import HTTPError

# -------------------- SETUP ----------------------- #

game: Optional[Game] = None

# initialize a flask server
app = Flask(__name__,
            static_url_path="",
            static_folder = "../dist",
            template_folder = "../dist")

# setup cross-origin resource sharing (for development)
CORS(app, resources={r'/*': {'origins': '*'}})

# -------------------- ERRORS ----------------------- #

@app.errorhandler(HTTPError)
def error_handler(error):
    """apply an error handler for returning neat request errors"""
    return jsonify({
        'success': False,
        'error': {
            'type': error.__class__.__name__,
            'code': error.status_code,
            'message': error.message
        }
    }), error.status_code

# -------------------- CONTROLLERS ----------------------- #

@app.route('/', methods=['GET'])
def home():
    """serve built static html from the root"""
    return render_template('static/index.html')

@app.route('/game', methods=['POST'])
def create_game():
    """create a new game instance and cache globally"""
    global game
    # fetch the post data, who is starting the game
    data = request.get_json()
    # create a new game
    game = Game(int(data["player_id"]),
                int(data["difficulty"]))
    # send back to client
    return jsonify({
        "finished": game.winner,
        "current_player": game.current_player_id(),
        "board": game.state_to_json()
    }), 200

@app.route('/moves/<player_id>', methods=['GET'])
def list_moves(player_id):
    """get all player moves"""
    global game
    # validate current game state and parameters
    if not game:
        raise HTTPError(message='game has not been created',
                        status_code=404)
    if int(player_id) != game.current_player_id():
        raise HTTPError(message=f"its not your move",
                        status_code=400)
    # list all available moves for this player
    moves = game.available_moves_to_json(game.current_state(),
                                         game.current_player_id())
    # send back to client
    return jsonify({
        "player_id": player_id,
        "moves": moves
    }), 200

@app.route('/moves/<player_id>/search', methods=['GET'])
def search_moves(player_id):
    """search with minimax for the best moves to a specified depth"""
    global game
    # validate current game state and parameters
    if not game:
        raise HTTPError(message='game has not been created',
                        status_code=404)
    if not int(player_id) == game.current_player_id():
        raise HTTPError(message=f"its not your move",
                        status_code=400)
    # fetch optional query string arguments
    if request.args.get("max_depth"): max_depth = int(request.args.get("max_depth"))
    if request.args.get("max_evaluations"): max_evaluations = int(request.args.get("max_evaluations"))
    # search the game state for the best move
    search = Search(game,
                    max_depth=max_depth,
                    max_evaluations=max_evaluations)
    best_move = search.evaluate()
    return jsonify({
        "player_id": player_id,
        "winner": game.winner,
        "move": best_move.to_json() if best_move else None,
        "evaluations": search.static_evaluation_count
    }), 200

@app.route('/move/<player_id>', methods=['POST'])
def create_move(player_id):
    """create a player move"""
    global game
    if not game:
        raise HTTPError(message='game has not been created',
                        status_code=404)
    if not int(player_id) == game.current_player_id():
        raise HTTPError(message=f"its not your move",
                        status_code=400)
    # fetch json request
    data = request.get_json()
    source, destination = (int(data["source"]), int(data["destination"]))
    valid_move = None
    # validate (and potentially limit) the move
    for move in game.available_moves(game.current_state(),
                                     game.current_player_id()):
        # skip all moves where this isn't the correct source
        if move.get_source() != source:
            continue
        # find the move with the specified destination
        if limited_move := move.limit_by_destination(destination):
            valid_move = limited_move
    if not valid_move:
        raise HTTPError(message=f"{(source, destination)} is not a valid move",
                        status_code=400)
    # make the move
    valid_move.apply_chain(game.current_state(), game.current_player_id())
    game.update()
    # build a chain
    moves = []
    for move in valid_move.chain():
        moves.append([move.source, move.destination, move.captured])
    # send a confirmation response
    return jsonify({
        "winner": game.winner,
        "moves": moves,
        "board": game.state_to_json(),
        "current_player": game.current_player_id()
    }), 200

if __name__ == '__main__':
    app.run(debug=True,
            host='0.0.0.0')
```

---

```python
# server/tests.py

import sys
import os, pathlib
import pytest

sys.path.insert(0, "./server")

pytest.main()
```

---

```python
# server/main.py

import os
import sys
sys.path.insert(0, "./server")

import time
import threading
import webbrowser
from server.app import app

HOST = '0.0.0.0'
PORT = '5000'

if __name__ == '__main__':
    try:
        target = lambda: app.run(debug=True, use_reloader=False, host=HOST)
        threading.Thread(target=target).start()
        webbrowser.open(f"http://{HOST}:{PORT}")
    except KeyboardInterrupt:
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
```
