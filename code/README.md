# Pieces of Eight (Monkey Island Themed Draughts)

This implementation of a Draughts game with an AI is composed to two components, a JavaScript UI (client) and a Python back-end (server).

## Usage

The application comes with the UI pre-built, but if for any reason you need to build it, skip to [setup](#setup), then come back to this section. Python version used for development was `v3.8.11`.

```
# ensure necessary python dependencies
# flask is a lightweight web server and application framework
pip install flask flask-cors
pip install -U pytest

# run the tests
python tests.py

# start the app
python main.py
```

## Setup

If `dist/` is not available or the UI is having issues, you'll need to recompile the UI. This requires NodeJS to be installed and a package manager. We recommend installing [nvm](https://github.com/nvm-sh/nvm), followed by node `v16.5.0`, then proceeding with the following commands:

```
# install dependencies
npm --prefix ui install
# build the UI to the ./dist folder
npm --prefix ui run build
```

Now you can run the python main script as above. This should open your browser, or you can manually navigate to [http://0.0.0.0:5000](http://0.0.0.0:5000).
