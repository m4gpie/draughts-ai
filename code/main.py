import os
import sys
sys.path.insert(0, "./server")

import time
import threading
import webbrowser
from server.app import app

HOST = '0.0.0.0'
PORT = '5000'

if __name__ == '__main__':
    try:
        target = lambda: app.run(debug=True, use_reloader=False, host=HOST)
        threading.Thread(target=target).start()
        webbrowser.open(f"http://{HOST}:{PORT}")
    except KeyboardInterrupt:
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
