import Home from './views/Home.vue'
import Rules from './views/Rules.vue'
import Game from './views/Game.vue'

export default [
  { path: '/', name: 'home', component: Home },
  { path: '/rules', name: 'rules', component: Rules },
  { path: '/game', name: 'game', component: Game, props: true }
]
