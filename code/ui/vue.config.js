module.exports = {
  outputDir: '../dist',
  assetsDir: './static',
  indexPath: './static/index.html',
  pages: {
    index: {
      entry: 'main.js',
      template: 'static/index.html'
    }
  },
  devServer: {
    proxy: {
      '^/api': {
        target: 'http://localhost:5000',
        ws: true,
        changeOrigin: true
      }
    },
    overlay: {
      warnings: false,
      errors: false
    }
  }
}
