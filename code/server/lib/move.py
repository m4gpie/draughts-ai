from typing import Optional
from lib.local_types import State
from lib.constants import PLAYER_1, PLAYER_2, QUEEN, PAWN

class Move:
    """
    Move is a recursive data structure. When moving draughts pieces,
    a move may be singular, or have multiple steps when captures are
    made, and n captures can be completed in a single turn. These can
    fan out in a tree structure, as there may be multiple pathways
    available following a single capture. As such each move contains
    a reference to its parent, such that the leaf nodes can be traced
    back to form a chain of moves. A move instance caches the values
    of the pieces that are captured, such that moves can be easily
    reversed or undone. Moves also contain the logic that applies the
    promotion of a pawn to a queen, either through reaching the rear line
    or through regicide (capturing another queen). When a move is applied
    it makes changes to the provided state representations for each player
    """
    def __str__(self):
        return str((self.source, self.destination, self.captured))

    def __init__(self,
                 source: int,
                 destination: int,
                 captured: Optional[int] = None,
                 source_value: int = PAWN,
                 destination_value: int = PAWN,
                 captured_value: Optional[int] = None,
                 parent: Optional["Move"] = None):
        self.source = source
        self.destination = destination
        self.captured = captured
        self.source_value = source_value
        self.destination_value = destination_value
        self.captured_value = captured_value
        self.parent = parent

    def to_json(self):
        """
        return the root move source and
        the current move's destination
        as a dict for easy json conversion
        """
        return {
            "source": self.get_source(),
            "destination": self.destination
        }

    def get_source(self):
        """
        fetch the root move of a move
        """
        source = None
        move = self
        while move:
            source = move.source
            move = move.parent
        return source

    def get_source_and_destinations(self):
        """
        compile a tuple of the move source,
        and a sequential list of destinations
        """
        destinations = []
        source = None
        move = self
        while move:
            source = move.source
            destinations.append(move.destination)
            move = move.parent
        return source, destinations

    def limit_by_destination(self, destination):
        """
        return the move from a chain, limited by the destination
        """
        move = self
        while move:
            if move.destination == destination:
                return move
            move = move.parent

    def apply(self,
              state: State,
              player_id: int):
        """
        apply a single move to the current state
        caching any captured values for easy reversal
        """
        current_player, other_player = state
        self.source_value = current_player[self.source]
        # move to destination, crown queen if applicable
        destination_value = self.source_value
        # delete any captured pieces from other player
        if self.captured:
            self.captured_value = other_player[self.captured]
            del other_player[self.captured]
        # delete from source
        del current_player[self.source]
        # we become a queen whenever player 1 or player 2 hits the queens row
        # or through regicide, whenever we capture an opponent queen
        if ((player_id == PLAYER_1 and 56 <= self.destination <= 63) or
                (player_id == PLAYER_2 and 0 <= self.destination <= 7) or
                self.captured_value == QUEEN):
            destination_value = QUEEN
        self.destination_value = destination_value
        current_player[self.destination] = self.destination_value

    def apply_chain(self,
                    state: State,
                    player_id: int):
        """
        apply an entire move chain to the current state
        caching any captured values for easy reversal
        """
        current_player, other_player = state
        # delete any captured pieces from other player
        move: Optional[Move] = self
        # we need to figure out the source
        source = None
        crowned = False
        while move:
            # remove captures until we find the highest source
            if move.captured:
                move.captured_value = other_player[move.captured]
                del other_player[move.captured]
            # move to destination, crown queen if applicable including regicide
            if ((player_id == PLAYER_1 and 56 <= move.destination <= 63) or
                    (player_id == PLAYER_2 and 0 <= move.destination <= 7) or
                    move.captured_value == QUEEN):
                crowned = True
            source = move.source
            move = move.parent
        # delete from source
        destination_value = QUEEN if crowned else current_player[source]
        del current_player[source]
        current_player[self.destination] = destination_value

    def undo(self,
             state: State,
             player_id: int):
        """
        apply the reverse of a move to the current state
        """
        current_player, other_player = state
        # recover any captured pieces
        if self.captured and self.captured_value:
            other_player[self.captured] = self.captured_value
        # delete from destination
        del current_player[self.destination]
        # move back to source
        current_player[self.source] = self.source_value

    def undo_chain(self,
                   state: State,
                   player_id: int):
        """
        apply the reverse of a chain of moves to the current state
        """
        current_player, other_player = state
        # recover any captured pieces
        move: Optional[Move] = self
        # figure out the source and original value
        source = None
        source_value = None
        while move:
            # insert captures back in along the way
            if move.captured and move.captured_value:
                other_player[move.captured] = move.captured_value
            source = move.source
            source_value = move.source_value
            move = move.parent
        # delete from destination
        del current_player[self.destination]
        # move back to source
        current_player[source] = source_value

    def chain(self):
        """compile an entire move chain as a list"""
        return self.__get_chain__(self, [self])

    def __get_chain__(self, move, moves = []):
        """recurse to return a chain of moves as a list"""
        if move.parent == None:
            moves.reverse()
            return moves
        moves.append(move.parent)
        return self.__get_chain__(move.parent, moves)
