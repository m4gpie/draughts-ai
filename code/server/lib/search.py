import code
import random
from typing import Iterable, List, Optional, Tuple
from lib.game import Game
from lib.local_types import State
from lib.move import Move

Evaluation = Tuple[Move, int]

INF = float('inf')

class Search:
    """
    Search contains the code for performing search in an adversarial context using
    the minimax algorithm with alpha-beta pruning and a heuristic to compute the
    static evaluations at the search's leaf nodes and determine the score of a certain
    search pathway
    """
    def __init__(self,
                 game: Game,
                 max_depth: Optional[int] = None,
                 max_evaluations: Optional[int] = None):
        self.game = game
        self.max_depth = int(max_depth) if max_depth else self.__max_depth__()
        self.max_static_evaluations = int(max_evaluations) if max_evaluations else self.__max_evaluations__()
        self.static_evaluation_count = 0
        self.dynamic_evaluation_count = 0
        self.evaluations: List[Evaluation] = []

    def __max_depth__(self):
        """maximum depth according to game difficulty"""
        return {
            1: 1,
            2: 5,
            3: 10
        }[self.game.difficulty]

    def __max_evaluations__(self):
        """
        maximum number of evaluations according to the game difficulty
        """
        return {
            1: 250,
            2: 10_000,
            3: 500_000
        }[self.game.difficulty]

    def evaluate(self) -> Optional[Move]:
        """
        evaluate calls the minimax algorithm using alpha-beta pruning
        to determine the best move for the current player given the current state
        """
        state = self.game.clone_state()
        self.minimax_alpha_beta(state, self.game.current_player_id())
        # if there are numerous pathways to a best score
        # then return all of them
        best_move = None
        best_score = -INF
        for move, score in self.evaluations:
            if best_score < score:
                best_score = score
                best_move = move
        # reset the evaluations
        self.evaluations.clear()
        # and return the best moves
        return best_move

    def static_evaluation(self,
                          state: State,
                          player_id: int,
                          depth) -> Optional[int]:
        """our static evaluation results are determined by our heuristic function h(n)"""
        return self.heuristic(state,
                              player_id,
                              depth)

    def heuristic(self,
                  state: State,
                  player_id: int,
                  depth: int) -> Optional[int]:
        """
        both players seek to increase the difference between the total value its
        their own pieces and their opponents, incentivising captures and crowning
        """
        current_player_state, other_player_state = state
        # first check if we've hit the maximum depth
        if depth == self.max_depth and player_id == self.game.current_player_id():
            # current i.e. maximising player seeks to increase the difference between
            # the total value of pieces it has, and the total value of pieces its opponent has
            return sum(current_player_state.values()) - sum(other_player_state.values())
            # return len(current_player_state) - len(other_player_state)
        elif depth == self.max_depth and player_id == self.game.other_player_id():
            # other i.e. minimising player seeks to decrease the difference between
            # the total value of pieces it has and the total value of pieces its opponent has
            return -(sum(current_player_state.values()) - sum(other_player_state.values()))
        # if none of these applies, keep searching
        return None

    def minimax_alpha_beta(self,
                           state: State,
                           player_id: int,
                           alpha: float = -INF,
                           beta: float = INF,
                           depth: int = 0):
        """
        minimax is a search algorithm for an adversarial context, seeking to find 
        the best move to a certain depth. The current player seeks to maximise the
        utility of a given move, while the other player (opponent) will seek to
        minimise the other player's result. alpha and beta prune the search so we 
        only expand nodes that will give good outcomes for the maximising player
        """
        # return heuristic value if evaluating at the leaf nodes
        evaluation = self.static_evaluation(state, player_id, depth)
        if (evaluation != None):
            self.static_evaluation_count += 1
            return evaluation
        # best score defaults to +/- 12, the worst possible outcome for min/max
        best_score = -12 if player_id == self.game.current_player_id() else 12
        # successor function fetches all valid moves for the player in question, given current state
        for move in self.game.available_moves(state, player_id):
            # regardless of whether we're maximising or minimising, the move(s) are made
            move.apply_chain(state, player_id)
            # setup state for deepening
            next_state = tuple(reversed(state))
            # maximising player
            if player_id == self.game.current_player_id():
                # next recursion minimises for other player, recurse until static evaluation
                current_score = self.minimax_alpha_beta(next_state,
                                                        self.game.other_player_id(),
                                                        alpha,
                                                        beta,
                                                        depth=depth + 1)
                # perform dynamic evaluation
                best_score = max(best_score, current_score)
                alpha = max(current_score, alpha)
                self.dynamic_evaluation_count += 1
                # if first depth layer, store evaluations to inform final decision
                if depth == 0:
                    self.evaluations.append((move, current_score))
            # minimising player
            elif player_id == self.game.other_player_id():
                # next recursion maximises for current player, recurse until static eval
                current_score = self.minimax_alpha_beta(next_state,
                                                        self.game.current_player_id(),
                                                        alpha,
                                                        beta,
                                                        depth=depth + 1)
                # perform dynamic evaluation
                best_score = min(best_score, current_score)
                beta = min(current_score, beta)
                self.dynamic_evaluation_count += 1
            # reverse the move(s)
            move.undo_chain(state, player_id)
            # don't explore unnecessary pathways (pruning) / return early if max evaluations are reached
            if ((alpha >= beta) or
                    (self.static_evaluation_count == self.max_static_evaluations)):
                break
        # return the best score
        return best_score
