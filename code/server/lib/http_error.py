class HTTPError(Exception):
    def __init__(self, message, status_code):
        self.message = message
        self.status_code = status_code

    def __str__(self):
        if self.status_code == 500:
            return f"Internal Server Error: {self.message}"
        elif self.status_code == 400:
            return f"Bad Request: {self.message}"
        elif self.status_code == 404:
            return f"Not Found: {self.message}"
