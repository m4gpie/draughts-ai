import code
import copy
import random
from typing import List, Iterable, Optional, Dict, Tuple
from lib.local_types import State, Player
from lib.constants import PLAYER_1, PLAYER_2, QUEEN, PAWN
from lib.move import Move

class Game:
    """
    Game contains all core game logic. This includes:

    - our game state representation is composed of two dictionaries storing each player's state
      the key is an integer corresponding to a valid tile on the game board,
      the value is the value of that piece, either a pawn or a queen.

    - our successor function is composed of three distinct functions,
      'neighbours' fetches all possible neighbouring tiles
      'captures' fetches move chains that correspond to all possible capture pathways
      'available_moves' returns all possible moves, including (and restricted by) captures

    - our goal check function which checks to see if a player has won the game is contained within 'update'
    """
    def __init__(self,
                 starting: int = PLAYER_2,
                 difficulty: int = 0) -> None:
        assert 0 <= starting <= 1, 'starting player must have an id of 0 or 1'
        self.turn = starting
        self.difficulty = difficulty
        self.player_1: Player = {}
        self.player_2: Player = {}
        # generate starting state
        self.__initial_state__()
        # store in a list to fetch easily
        self.players: List[Player] = [self.player_1,
                                      self.player_2]
        # the winner is unassigned
        self.winner: Optional[int] = None

    def state_to_json(self):
        """
        returns a dictionary representation of the current board state
        """
        state = {}
        for n in range(64):
            state[n] = None
            if value := self.player_1.get(n):
                state[n] = { "player_id": PLAYER_1, "queen": value == QUEEN }
            elif value := self.player_2.get(n):
                state[n] = { "player_id": PLAYER_2, "queen": value == QUEEN }
        return state

    def available_moves_to_json(self,
                                current_state: State,
                                player_id: int):
        """
        returns a dictionary representation of the available moves for a given player and state
        """
        json: Dict[int, List] = {}
        for move in self.available_moves(current_state, player_id):
            source, destinations = move.get_source_and_destinations()
            if json.get(source):
                json[source].extend(destinations)
            else:
                json[source] = destinations
        return json

    def current_player_id(self) -> int:
        """
        current player id is either 0 or 1
        """
        return self.turn % 2

    def current_player(self) -> Player:
        """
        the player state dict for the current player
        """
        return self.players[self.current_player_id()]

    def other_player_id(self) -> int:
        """
        inverse of current player id
        """
        return abs(1 - self.current_player_id())

    def other_player(self) -> Player:
        """
        the player state dict for the other player / opponent
        """
        return self.players[self.other_player_id()]

    def current_state(self) -> State:
        """
        current state is a tuple where the current player is the first
        """
        if self.current_player_id() == PLAYER_1:
            return (self.player_1, self.player_2)
        else:
            return (self.player_2, self.player_1)

    def clone_state(self) -> State:
        """
        deep copy the current state for search
        """
        return copy.deepcopy(self.current_state())

    def select_move(self,
                    moves: List[Move],
                    is_random: bool = True):
        """
        either select randomly or take the first moves from a list of moves
        """
        return moves[random.randint(0, len(moves) - 1)] if is_random else moves[0]

    def update(self):
        """
        perform a goal check, i.e. the opponent has no remaining pieces, then increment the turn
        """
        if not len(self.player_1):
            self.winner = PLAYER_2
        elif not len(self.player_2):
            self.winner = PLAYER_1
        self.turn += 1

    def neighbours(self,
                   source: int,
                   player_id: int,
                   is_queen: bool = False) -> Iterable[int]:
        """
        neighbouring squares are defined according to our state representation.
        The board is defined as indices between 0 and (8 x 8) - 1 = 63 (note that alternating
        odd and even squares cannot be occupied). neighbours function takes a source integer
        between 0 and 63 and returns all possible neighbours irrespective of whether
        neighbouring squares are occupied or not. Player 1 moves down the board, so all
        neighbouring squares must be later in the sequence, we add 7 or 9 and ensure that
        only the next row of 8 numbers is valid. Player 2 moves up the board, so all neighbouring
        squares must be earlier in the sequence, we subtract 7 or 9 and ensure the previous
        row of 8 numbers is valid. queens can move up or down, so if the piece at the source
        is a queen, we treat as if it is both players.
        """
        # player 1 can only move up the board, all queens can move up
        i = int(source / 8)
        if player_id == PLAYER_1 or is_queen:
            # upper bound of the board is 63
            # first term calculates the relevant square, second term ensures
            # it remains on a neighbouring row
            if (left := source + 7) <= 63 and int(left / 8) - i == 1:
                yield left
            if (right := source + 9) <= 63 and int(right / 8) - i == 1:
                yield right
        # player 2 can only move down the board, all queens can move down
        if player_id == PLAYER_2 or is_queen:
            # lower bound of the board is 0
            if (right := source - 7) >= 0 and int(right / 8) - i == -1:
                yield right
            if (left := source - 9) >= 0 and int(left / 8) - i == -1:
                yield left

    def captures(self,
                 move: Move,
                 state: State,
                 player_id: int,
                 moves: List[Move] = []):
        """
        starting at the move's destination (the projected current location for the piece)
        recursively discover capture moves, collecting only the leaf nodes (moves are a nested
        data structure and can easily be reconstituted to form a full move path)
        """
        # fetch state
        current_player_state, other_player_state = state
        source, value = move.destination, move.destination_value
        capturing = False
        is_queen = (value == QUEEN)
        # fetch all possible neighbouring squares
        for destination in self.neighbours(source, player_id, is_queen):
            # if a neighbour is occupied by an opponent, check if the next square is empty
            if destination in other_player_state.keys():
                # find next position along the same axis
                i, j = int(source / 8), source % 8
                k, l = int(destination / 8), destination % 8
                m, n = k + (k - i), l + (l - j)
                captured = destination
                destination = m * 8 + n
                # check the new destination is within the bounds
                if ((0 <= destination <= 63) and
                        # and the original destination is not a border square
                        (0 < l < 7) and
                        # and its unoccupied
                        destination not in current_player_state and
                        destination not in other_player_state):
                    capturing = True
                    # build and yield a move
                    next_move = Move(source=source,
                                     destination=destination,
                                     captured=captured,
                                     parent=move)
                    # temporarily apply the move
                    next_move.apply(state, player_id)
                    # recurse until maximum depth
                    self.captures(next_move, state, player_id, moves)
                    # undo the move
                    next_move.undo(state, player_id)
                    # yield the original capture move
        if not capturing:
            # we've hit maximum depth, we can
            # do no more, append move to leaf nodes
            moves.append(move)
        return moves

    def available_moves(self,
                        state: State,
                        player_id: int) -> Iterable[Move]:
        """
        the available moves or successor function for the current state takes into consideration all current
        pieces of a given player, and searches for possible locations to move according to what constitutes a
        valid transition. Transitions / neighbouring squares are valid when they are unoccupied.
        If a square is occupied by an opponent, and the square beyond it along the same axis is unoccupied,
        this too is a valid move, and a capture is available. When a capture is available, we then only
        consider other captures as valid moves, forcing players to perform capture moves.
        """
        # fetch state
        current_player_state, other_player_state = state
        # store all moves
        moves: List[Move] = []
        # store capture moves separately, if this is populated we return only capture moves
        capturing: List[Move] = []
        # for every piece that player has
        for source in list(current_player_state.keys()):
            # check if its a queen
            is_queen = (current_player_state[source] == QUEEN)
            # fetch all neighbour squares
            for destination in self.neighbours(source, player_id, is_queen):
                # check if they're valid
                # is this occupied by our own piece?
                if destination in current_player_state.keys():
                    pass
                # is an opponent there?
                elif destination in other_player_state.keys():
                    # find next position along the same axis
                    i, j = int(source / 8), source % 8
                    k, l = int(destination / 8), destination % 8
                    m, n = k + (k - i), l + (l - j)
                    captured = destination
                    destination = m * 8 + n
                    # check the new destination is within the bounds
                    if ((0 <= destination <= 63) and
                            # and the original destination is not a border square
                            (0 < l < 7) and
                            # and its unoccupied
                            destination not in current_player_state and
                            destination not in other_player_state):
                        # build and yield a root node move
                        move = Move(source=source,
                                    destination=destination,
                                    captured=captured)
                        # temporarily apply the move
                        move.apply(state, player_id)
                        # recurse until there are no more capture pathways
                        capture_move_chains = self.captures(move, state, player_id, [])
                        # merge into list of capturing moves (leaf nodes)
                        capturing.extend(capture_move_chains)
                        # undo the move
                        move.undo(state, player_id)
                # non-capture moves only valid when there are no captures (forced capture)
                elif not len(capturing):
                    move = Move(source, destination)
                    moves.append(move)
        # return capturing only if there are any, otherwise return all possible moves
        return capturing if len(capturing) else moves

    def __initial_state__(self):
        """
        build initial state based on 8x8 starting grid
        alternating odd and even rows and columns
        """
        for n in range(64):
            # transform into a discrete co-ordinate
            i = int(n / 8)
            j = n % 8
            # alternating odd / even
            if ((i % 2 == 0 and j % 2 == 1) or
                    (i % 2 == 1 and j % 2 == 0)):
                # player 1 on the first three rows
                if n < 24:
                    self.player_1[n] = 1
                # player 2 the last three rows
                elif n > 39:
                    self.player_2[n] = 1
