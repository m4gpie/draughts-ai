from typing import Iterable, Optional, Tuple, Dict
# A player is a list of draught locations and their values
Player = Dict[int, int]
# State is a tuple of the two players
State = Tuple[Player, Player]
