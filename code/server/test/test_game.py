import code
from lib.game import Game
from lib.constants import PLAYER_1, PLAYER_2, QUEEN, PAWN

def test_player_1_neighbours():
    game = Game()

    assert list(game.neighbours(44, PLAYER_1)) == [51, 53]

    assert list(game.neighbours(0, PLAYER_1)) == [9], \
        'player 1 draughts in nearside corner (i = even) have only one diagonal neighbour'

    assert list(game.neighbours(5, PLAYER_1)) == [12, 14], \
        'player 1 draughts in nearside corner (i = odd) have two diagonal neighbours'

    assert list(game.neighbours(9, PLAYER_1)) == [16, 18], \
        'player 1 draughts in center have two diagonal neighbours'

    assert list(game.neighbours(9, PLAYER_1, is_queen=True)) == [16, 18, 2, 0], \
        'player 1 queens in center have four diagonal neighbours'

    assert list(game.neighbours(15, PLAYER_1)) == [22], \
        'player 1 draughts on edge (i = odd) have one diagonal neighbour'

    assert list(game.neighbours(47, PLAYER_1)) == [54], \
        'player 1 draughts on edge (i = even) have one diagonal neighbour'

    assert list(game.neighbours(47, PLAYER_1, is_queen=True)) == [54, 38], \
        'player 1 queens on edge have two diagonal neighbours'

    assert list(game.neighbours(62, PLAYER_1)) == [], \
        'player 1 draughts in farside corner (i = odd) have no neighbours'

    assert list(game.neighbours(62, PLAYER_1, is_queen=True)) == [55, 53], \
        'player 1 queens in farside corner (i = odd) have one neighbour'

    assert list(game.neighbours(56, PLAYER_1)) == [], \
        'player 1 draughts in farside corner (i = even) have no neighbours'

    assert list(game.neighbours(56, PLAYER_1, is_queen=True)) == [49], \
        'player 1 queens in farside corner (i = even) have one neighbour'

    assert list(game.neighbours(59, PLAYER_1)) == [], \
        'player 1 draughts on farside in center have no neighbours'

    assert list(game.neighbours(60, PLAYER_1, is_queen=True)) == [53, 51], \
        'player 1 queens on farside in center have two neighbours'

def test_player_2_neighbours():
    game = Game()

    assert list(game.neighbours(60, PLAYER_2)) == [53, 51], \
        'player 2 draughts on nearside in center have two neighbours'

    assert list(game.neighbours(56, PLAYER_2)) == [49], \
        'player 2 draughts in nearside corner (i = odd) have two diagonal neighbours'

    assert list(game.neighbours(62, PLAYER_2)) == [55, 53], \
        'player 2 draughts in nearside corner (i = even) have only one diagonal neighbour'

    assert list(game.neighbours(53, PLAYER_2)) == [46, 44], \
        'player 2 draughts in center have two diagonal neighbours'

    assert list(game.neighbours(53, PLAYER_2, is_queen=True)) == [60, 62, 46, 44], \
        'player 2 queens in center have four diagonal neighbours'

    assert list(game.neighbours(47, PLAYER_2)) == [38], \
        'player 2 draughts on edge (i = odd) have one diagonal neighbour'

    assert list(game.neighbours(40, PLAYER_2)) == [33], \
        'player 2 draughts on edge (i = even) have one diagonal neighbour'

    assert list(game.neighbours(40, PLAYER_2, is_queen=True)) == [49, 33], \
        'player 2 queens on edge have two diagonal neighbours'

    assert list(game.neighbours(0, PLAYER_2)) == [], \
        'player 2 draughts in farside corner (i = odd) have no neighbours'

    assert list(game.neighbours(0, PLAYER_2, is_queen=True)) == [9], \
        'player 2 queens in farside corner (i = odd) have one neighbour'

    assert list(game.neighbours(6, PLAYER_2)) == [], \
        'player 2 draughts in farside corner (i = even) have no neighbours'

    assert list(game.neighbours(5, PLAYER_2, is_queen=True)) == [12, 14], \
        'player 2 queens in farside corner (i = even) have two neighbours'

    assert list(game.neighbours(1, PLAYER_2)) == [], \
        'player 2 draughts on farside in center have no neighbours'

    assert list(game.neighbours(1, PLAYER_2, is_queen=True)) == [8, 10], \
        'player 2 queens on farside in center have two neighbours'

def test_player_1_available_moves():
    game = Game(starting=0)
    # at the beginning of the game, available moves for player one are:
    moves = [(move.source, move.destination, move.captured)
             for move in game.available_moves(game.clone_state(), game.current_player_id())]
    assert moves == [(17, 24, None),
                     (17, 26, None),
                     (19, 26, None),
                     (19, 28, None),
                     (21, 28, None),
                     (21, 30, None),
                     (23, 30, None)], 'returns a list of all valid player 1 successor moves'

    game.player_1[28] = game.player_1[19]
    del game.player_1[19]

    moves = [(move.source, move.destination, move.captured)
             for move in game.available_moves(game.clone_state(), game.current_player_id())]
    assert moves == [(10, 19, None),
                     (12, 19, None),
                     (17, 24, None),
                     (17, 26, None),
                     (21, 30, None),
                     (23, 30, None),
                     (28, 35, None),
                     (28, 37, None)], 'returns a list of all valid player 1 successor moves'

def test_player_2_available_moves():
    game = Game(starting=1)
    # at the beginning of the game, available moves for player two are:
    moves = [(move.source, move.destination, move.captured)
             for move in game.available_moves(game.clone_state(), game.current_player_id())]
    assert  moves == [(40, 33, None),
                      (42, 35, None),
                      (42, 33, None),
                      (44, 37, None),
                      (44, 35, None),
                      (46, 39, None),
                      (46, 37, None)], 'returns a list of all valid player 2 successor moves'

    game.player_2[33] = game.player_2[42]
    del game.player_2[42]

    moves = [(move.source, move.destination, move.captured)
             for move in game.available_moves(game.clone_state(), game.current_player_id())]
    assert  moves == [(44, 37, None),
                      (44, 35, None),
                      (46, 39, None),
                      (46, 37, None),
                      (49, 42, None),
                      (51, 42, None),
                      (33, 26, None),
                      (33, 24, None)], 'returns a list of all valid player 2 successor moves'

def test_forced_capture_single_player_1():
    game = Game(starting=0)
    # move and increment turn
    game.player_1[28] = game.player_1[19]
    del game.player_1[19]
    # move a piece to a capturable location
    game.player_2[35] = game.player_2[42]
    del game.player_2[42]
    # only one available move, a forced capture
    moves = [(move.source, move.destination, move.captured)
             for move in game.available_moves(game.clone_state(), game.current_player_id())]
    assert moves == [(28, 42, 35)], 'returns a single capture successor move'

def test_forced_capture_choice_player_1():
    game = Game(starting=0)
    # move and increment turn
    game.player_1[28] = game.player_1[19]
    del game.player_1[19]
    # move pieces to capturable locations
    game.player_2[35] = game.player_2[42]
    del game.player_2[42]
    game.player_2[37] = game.player_2[46]
    del game.player_2[46]
    # two available moves, a choice of forced captures
    moves = [(move.source, move.destination, move.captured)
             for move in game.available_moves(game.clone_state(), game.current_player_id())]
    assert moves == [(28, 42, 35),
                     (28, 46, 37)], 'returns a list of capture successor moves'

def test_forced_capture_choice_player_2():
    game = Game(starting=1)
    # move and increment turn
    game.player_1[28] = game.player_1[19]
    del game.player_1[19]
    # move pieces to capturable locations
    game.player_2[35] = game.player_2[42]
    del game.player_2[42]
    game.player_2[37] = game.player_2[46]
    del game.player_2[46]
    game.player_1[42] = game.player_1[28]
    del game.player_1[28]
    del game.player_2[35]
    moves = [(move.source, move.destination, move.captured)
             for move in game.available_moves(game.clone_state(), game.current_player_id())]
    assert moves == [(49, 35, 42), (51, 33, 42)], 'returns a list of unique capture successor moves'

def test_forced_capture_chain():
    game = Game(starting=0)
    # move player 1 into position
    game.player_1[28] = game.player_1[19]
    del game.player_1[19]
    # move player 2 into position and delete piece on backrow
    game.player_2[35] = game.player_2[42]
    del game.player_2[42]
    del game.player_2[60]
    moves = game.available_moves(game.clone_state(), game.current_player_id())
    # fetch the only available move
    move = moves[0]
    # this move has a chain, there are two hops we can make
    chain = [(move.source, move.destination, move.captured) for move in move.chain()]
    assert chain == [(28, 42, 35), (42, 60, 51)], 'returns a chain of capture successor moves'

def test_queen_capture():
    players = [{ 7: PAWN, 19: PAWN, 21: PAWN, 23: PAWN },
               { 10: QUEEN, 46: PAWN, 51: PAWN, 55: PAWN, 60: PAWN, 62: PAWN }]
    game = Game(starting=1)
    game.player_1 = players[0]
    game.player_2 = players[1]
    game.players = players
    moves = game.available_moves(game.clone_state(), game.current_player_id())
    move = moves[0]
    chain = [(move.source, move.destination, move.captured) for move in move.chain()]
    assert chain == [(10, 28, 19), (28, 14, 21)], 'returns a chain of queen capture successor moves'

def test_queen_double_capture_reverse_direction():
    players = [{ 24: PAWN, 49: QUEEN },
               { 5: QUEEN, 26: QUEEN, 37: QUEEN, 39: PAWN, 42: PAWN, 44: PAWN, 46: PAWN, 53: PAWN, 55: PAWN }]
    game = Game(starting=0)
    game.player_1 = players[0]
    game.player_2 = players[1]
    game.players = players
    moves = game.available_moves(game.clone_state(), game.current_player_id())
    move = moves[0]
    chain = [(move.source, move.destination, move.captured) for move in move.chain()]
    assert chain == [(49, 35, 42), (35, 17, 26)], 'returns a chain of queen capture successor moves'
