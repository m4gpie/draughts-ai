import code
from flask import Flask, jsonify, request
from flask import render_template
from flask_cors import CORS
from typing import Optional, Dict, List
from lib.game import Game
from lib.search import Search
from lib.constants import PAWN, QUEEN
from lib.local_types import State
from lib.http_error import HTTPError

# -------------------- SETUP ----------------------- #

game: Optional[Game] = None

# initialize a flask server
app = Flask(__name__,
            static_url_path="",
            static_folder = "../dist",
            template_folder = "../dist")

# setup cross-origin resource sharing (for development)
CORS(app, resources={r'/*': {'origins': '*'}})

# -------------------- ERRORS ----------------------- #

@app.errorhandler(HTTPError)
def error_handler(error):
    """apply an error handler for returning neat request errors"""
    return jsonify({
        'success': False,
        'error': {
            'type': error.__class__.__name__,
            'code': error.status_code,
            'message': error.message
        }
    }), error.status_code

# -------------------- CONTROLLERS ----------------------- #

@app.route('/', methods=['GET'])
def home():
    """serve built static html from the root"""
    return render_template('static/index.html')

@app.route('/game', methods=['POST'])
def create_game():
    """create a new game instance and cache globally"""
    global game
    # fetch the post data, who is starting the game
    data = request.get_json()
    # create a new game
    game = Game(int(data["player_id"]),
                int(data["difficulty"]))
    # send back to client
    return jsonify({
        "finished": game.winner,
        "current_player": game.current_player_id(),
        "board": game.state_to_json()
    }), 200

@app.route('/moves/<player_id>', methods=['GET'])
def list_moves(player_id):
    """get all player moves"""
    global game
    # validate current game state and parameters
    if not game:
        raise HTTPError(message='game has not been created',
                        status_code=404)
    if int(player_id) != game.current_player_id():
        raise HTTPError(message=f"its not your move",
                        status_code=400)
    # list all available moves for this player
    moves = game.available_moves_to_json(game.current_state(),
                                         game.current_player_id())
    # send back to client
    return jsonify({
        "player_id": player_id,
        "moves": moves
    }), 200

@app.route('/moves/<player_id>/search', methods=['GET'])
def search_moves(player_id):
    """search with minimax for the best moves to a specified depth"""
    global game
    # validate current game state and parameters
    if not game:
        raise HTTPError(message='game has not been created',
                        status_code=404)
    if not int(player_id) == game.current_player_id():
        raise HTTPError(message=f"its not your move",
                        status_code=400)
    # search the game state for the best move
    search = Search(game,
                    max_depth=request.args.get("max_depth"),
                    max_evaluations=request.args.get("max_evaluations"))
    best_move = search.evaluate()
    return jsonify({
        "player_id": player_id,
        "winner": game.winner,
        "move": best_move.to_json() if best_move else None,
        "evaluations": search.static_evaluation_count
    }), 200

@app.route('/move/<player_id>', methods=['POST'])
def create_move(player_id):
    """create a player move"""
    global game
    if not game:
        raise HTTPError(message='game has not been created',
                        status_code=404)
    if not int(player_id) == game.current_player_id():
        raise HTTPError(message=f"its not your move",
                        status_code=400)
    # fetch json request
    data = request.get_json()
    source, destination = (int(data["source"]), int(data["destination"]))
    valid_move = None
    # validate (and potentially limit) the move
    for move in game.available_moves(game.current_state(),
                                     game.current_player_id()):
        # skip all moves where this isn't the correct source
        if move.get_source() != source:
            continue
        # find the move with the specified destination
        if limited_move := move.limit_by_destination(destination):
            valid_move = limited_move
    if not valid_move:
        raise HTTPError(message=f"{(source, destination)} is not a valid move",
                        status_code=400)
    # make the move
    valid_move.apply_chain(game.current_state(), game.current_player_id())
    game.update()
    # build a chain
    moves = []
    for move in valid_move.chain():
        moves.append([move.source, move.destination, move.captured])
    # send a confirmation response
    return jsonify({
        "winner": game.winner,
        "moves": moves,
        "board": game.state_to_json(),
        "current_player": game.current_player_id()
    }), 200

if __name__ == '__main__':
    app.run(debug=True,
            host='0.0.0.0')
